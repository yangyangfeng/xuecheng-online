package com.xuecheng.plus.content.api;

import com.xuecheng.plus.base.exception.ValidationGroups;
import com.xuecheng.plus.base.model.PageParams;
import com.xuecheng.plus.base.model.PageResult;
import com.xuecheng.plus.content.model.dto.AddCourseDto;
import com.xuecheng.plus.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.plus.content.model.dto.EditCourseDto;
import com.xuecheng.plus.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.plus.content.model.po.CourseBase;
import com.xuecheng.plus.content.service.CourseBaseInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.api
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-14  22:46
 * @Description: TODO
 * @Version: 1.0
 **/
@Api(value = "课程信息管理接口",tags = "课程信息管理接口")
@RestController
public class CourceBaseInfoController {

    @Autowired
    CourseBaseInfoService courseBaseInfoService;

    // url中的属性和请求体的参数，解析方式不同：

    /**
     *  @PathVariable是url的一部分
     *  @RequestBody请求体的部分
     */

    @ApiOperation("课程查询接口")
    @PostMapping("/course/list")
    public PageResult<CourseBase> list(PageParams pageParams, @RequestBody(required=false) QueryCourseParamsDto queryCourseParamsDto) {
        PageResult<CourseBase> courseBasePageResult = courseBaseInfoService.queryCourseBaseList(pageParams, queryCourseParamsDto);
        return courseBasePageResult;
    }

    @ApiOperation("新增课程")
    @PostMapping("/course")
    public CourseBaseInfoDto createCourseBase(@RequestBody @Validated(ValidationGroups.Inster.class) AddCourseDto addCourseDto){
        //获取到用户所属机构的id
        Long companyId = 1232141425L;
        CourseBaseInfoDto courseBase = courseBaseInfoService.createCourseBase(companyId, addCourseDto);
        return courseBase;
    }

    @ApiOperation("根据课程id查询接口")
    @GetMapping("/course/{courseId}")
    public CourseBaseInfoDto getCourseBaseById(@PathVariable Long courseId){
        CourseBaseInfoDto courseBaseInfo = courseBaseInfoService.getCourseBaseInfo(courseId);
        return courseBaseInfo;
    }

    @ApiOperation("修改课程")
    @PutMapping("/course")
    public CourseBaseInfoDto modifyCourseBase(@RequestBody @Validated(ValidationGroups.Update.class) EditCourseDto editCourseDto){
        //获取到用户所属机构的id
        Long companyId = 1232141425L;
        CourseBaseInfoDto courseBaseInfoDto = courseBaseInfoService.updateCourseBase(companyId, editCourseDto);
        return courseBaseInfoDto;
    }
}
