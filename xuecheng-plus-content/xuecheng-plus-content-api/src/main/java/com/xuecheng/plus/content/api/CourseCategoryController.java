package com.xuecheng.plus.content.api;

import com.xuecheng.plus.content.model.dto.CourseCategoryTreeDto;
import com.xuecheng.plus.content.service.CourseCategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.api
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-25  08:04
 * @Description: TODO 课程分类相关接口
 * @Version: 1.0
 **/
@Api(value = "课程分类接口",tags = "课程分类接口")
@RestController
public class CourseCategoryController {

    @Autowired
    CourseCategoryService courseCategoryService;

    @GetMapping("/course-category/tree-nodes")
    public List<CourseCategoryTreeDto> queryTreeNodes() {
        return courseCategoryService.queryTreeNodes("1");
    }
}
