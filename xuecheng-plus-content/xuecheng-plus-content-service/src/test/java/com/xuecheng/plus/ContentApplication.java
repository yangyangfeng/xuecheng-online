package com.xuecheng.plus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-25  22:32
 * @Description: 内容管理服务启动类
 * @Version: 1.0
 **/
@SpringBootApplication
public class ContentApplication {
    public static void main(String[] args) {
        SpringApplication.run(ContentApplication.class, args);
    }
}

