package com.xuecheng.plus.content;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.xuecheng.plus.content.mapper.CourseBaseMapper;
import com.xuecheng.plus.content.service.CourseBaseInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Properties;

@SpringBootTest
class XuechengPlusContentServiceApplicationTests {

    @Autowired
    CourseBaseMapper courseBaseMapper;

    @Autowired
    CourseBaseInfoService courseBaseInfoService;


    @Test
    void contextLoads() {
        try {
            Properties properties = new Properties();
            properties.put("serverAddr", "127.0.0.1:8848");
            properties.put("namespace", "d30c3e20-af6b-4bd5-9993-d4a9eacaedad");
            ConfigService configService = NacosFactory.createConfigService(properties);
            String contentInfo = configService.getConfig("content-api-dev.yml", "xuecheng-plus-project", 1000L);
//            String contentInfo = configService.getConfig("media-api-dev.yaml", "xuecheng-plus-project", 1000L);
            System.out.println("contentInfo:" + contentInfo);
        } catch (NacosException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testCourseBase() {
//        courseBaseMapper.selectById()
    }

}
