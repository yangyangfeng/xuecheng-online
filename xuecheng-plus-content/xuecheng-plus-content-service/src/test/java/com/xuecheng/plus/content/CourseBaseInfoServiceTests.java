package com.xuecheng.plus.content;

import com.xuecheng.plus.base.model.PageParams;
import com.xuecheng.plus.base.model.PageResult;
import com.xuecheng.plus.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.plus.content.model.po.CourseBase;
import com.xuecheng.plus.content.service.CourseBaseInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-19  16:08
 * @Description: TODO
 * @Version: 1.0
 **/
@SpringBootTest
public class CourseBaseInfoServiceTests {

    @Autowired
    CourseBaseInfoService courseBaseInfoService;

    @Test
    public void testCourseBaseInfoService() {

        //查询条件
        QueryCourseParamsDto courseParamsDto = new QueryCourseParamsDto();
        courseParamsDto.setCourseName("java");//课程名称查询条件
        courseParamsDto.setAuditStatus("202004");//202004表示课程审核通过
        //分页参数对象
        PageParams pageParams = new PageParams();
        pageParams.setPageNo(2L);
        pageParams.setPageSize(2L);

        PageResult<CourseBase> courseBasePageResult = courseBaseInfoService.queryCourseBaseList(pageParams, courseParamsDto);
        System.out.println(courseBasePageResult);

    }
}