package com.xuecheng.plus.content.service;

import com.xuecheng.plus.content.model.dto.SaveTeachplanDto;
import com.xuecheng.plus.content.model.dto.TeachplanDto;

import java.util.List;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.service
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-26  11:07
 * @Description: TODO 课程计划管理相关的接口
 * @Version: 1.0
 **/
public interface TeachplanService {

    /**
     * 根据课程id查询课程计划
     *
     * @param courseId 课程计划
     * @return
     */
    public List<TeachplanDto> findTeachplanTree(Long courseId);

    /**
     * 新增/修改/保存课程计划
     *
     * @param saveTeachplanDto
     */
    public void saveTeachplan(SaveTeachplanDto saveTeachplanDto);


    /**
     * @param bindTeachplanMediaDto
     * @return com.xuecheng.content.model.po.TeachplanMedia
     * @description 教学计划绑定媒资
     * @author Mr.M
     * @date 2022/9/14 22:20
     */
//    public void associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto);
}
