package com.xuecheng.plus.content.service;

import com.xuecheng.plus.base.model.PageParams;
import com.xuecheng.plus.base.model.PageResult;
import com.xuecheng.plus.content.model.dto.AddCourseDto;
import com.xuecheng.plus.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.plus.content.model.dto.EditCourseDto;
import com.xuecheng.plus.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.plus.content.model.po.CourseBase;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plusc.ontent.service
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-14  22:48
 * @Description: TODO 课程信息管理接口
 * @Version: 1.0
 **/
public interface CourseBaseInfoService {

    /**
     * 课程分页查询
     * @param pageParams
     * @param courseParamsDto
     * @return
     */
    public PageResult<CourseBase> queryCourseBaseList(PageParams pageParams, QueryCourseParamsDto courseParamsDto);

    /**
     * 新增课程
     * @param companyId 机构id
     * @param addCourseDto 课程信息
     * @return 课程详细信息
     */
    public CourseBaseInfoDto createCourseBase(Long companyId, AddCourseDto addCourseDto);

    /**
     * 根据课程id查询课程信息
     * @param courseId 课程id
     * @return 课程详细信息
     */
    public CourseBaseInfoDto getCourseBaseInfo(Long courseId);

    /**
     * 修改课程
     * @param companyId 机构id
     * @param editCourseDto 修改课程信息
     * @return 课程详细信息
     */
    public CourseBaseInfoDto updateCourseBase(Long companyId, EditCourseDto editCourseDto);
}
