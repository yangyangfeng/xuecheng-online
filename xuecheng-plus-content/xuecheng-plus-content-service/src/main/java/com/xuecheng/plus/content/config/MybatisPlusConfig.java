package com.xuecheng.plus.content.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.config
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-19  13:10
 * @Description: TODO mybatis 分页拦截器：分页原理是利用ThreadLocal保存分页参数，拦截执行的SQl语句，拼接SQL语句时加上limt属性
 * @Version: 1.0
 **/

@Configuration
@MapperScan("com.xuecheng.plus.content.mapper")
public class MybatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

}
