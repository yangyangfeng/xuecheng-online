package com.xuecheng.plus.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.plus.content.model.dto.CourseCategoryTreeDto;

import java.util.List;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.service
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-25  08:33
 * @Description: TODO
 * @Version: 1.0
 **/
public interface CourseCategoryService{
    /**
     * 课程分类树形结构查询
     *
     * @return
     */
    public List<CourseCategoryTreeDto> queryTreeNodes(String id);
}
