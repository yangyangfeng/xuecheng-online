package com.xuecheng.plus.content.model.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.model.dto
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-14  22:37
 * @Description: TODO 课程查询条件模型类
 * @Version: 1.0
 **/
@Data
@ToString
public class QueryCourseParamsDto {

    //审核状态
    private String auditStatus;
    //课程名称
    private String courseName;
    //发布状态
    private String publishStatus;

}
