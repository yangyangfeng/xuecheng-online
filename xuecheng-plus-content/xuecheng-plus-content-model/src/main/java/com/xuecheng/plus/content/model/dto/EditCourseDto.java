package com.xuecheng.plus.content.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.model.dto
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-26  10:39
 * @Description: TODO
 * @Version: 1.0
 **/
@Data
public class EditCourseDto extends AddCourseDto{

    @ApiModelProperty(value = "课程id", required = true)
    private Long id;
}
