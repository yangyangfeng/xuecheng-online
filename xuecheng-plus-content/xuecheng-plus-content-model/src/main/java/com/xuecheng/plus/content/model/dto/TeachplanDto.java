package com.xuecheng.plus.content.model.dto;

import com.xuecheng.plus.content.model.po.Teachplan;
import com.xuecheng.plus.content.model.po.TeachplanMedia;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.model.dto
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-26  11:03
 * @Description: TODO 课程计划信息
 * @Version: 1.0
 **/
@ToString
@Data
public class TeachplanDto extends Teachplan {

    //与媒资管理的信息
    private TeachplanMedia teachplanMedia;

    //小章节list
    private List<TeachplanDto> teachPlanTreeNodes;
}
