package com.xuecheng.plus.content.model.dto;

import com.xuecheng.plus.content.model.po.CourseCategory;

import java.io.Serializable;
import java.util.List;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.content.model.dto
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-25  08:02
 * @Description: TODO 目录树
 *
 * z目录分级组装可以采取两种方式：数据库的子连接（当查询的分类级数比较固定）、MySQL的递归查询 和程序的递归
 * @Version: 1.0
 **/
public class CourseCategoryTreeDto extends CourseCategory implements Serializable {

    public List<CourseCategoryTreeDto> getChildrenTreeNodes() {
        return childrenTreeNodes;
    }

    public void setChildrenTreeNodes(List<CourseCategoryTreeDto> childrenTreeNodes) {
        this.childrenTreeNodes = childrenTreeNodes;
    }

    // 子节点
    List<CourseCategoryTreeDto> childrenTreeNodes;
}
