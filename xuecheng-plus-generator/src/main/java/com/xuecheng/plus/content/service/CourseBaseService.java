package com.xuecheng.plus.content.service;

import com.xuecheng.plus.content.model.po.CourseBase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程基本信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2023-03-19
 */
public interface CourseBaseService extends IService<CourseBase> {

}
