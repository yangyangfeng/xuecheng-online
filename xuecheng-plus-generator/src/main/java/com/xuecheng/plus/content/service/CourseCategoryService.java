package com.xuecheng.plus.content.service;

import com.xuecheng.plus.content.model.po.CourseCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程分类 服务类
 * </p>
 *
 * @author itcast
 * @since 2023-03-19
 */
public interface CourseCategoryService extends IService<CourseCategory> {

}
