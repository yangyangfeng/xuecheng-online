package com.xuecheng.plus.content.service;

import com.xuecheng.plus.content.model.po.TeachplanMedia;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author itcast
 * @since 2023-03-19
 */
public interface TeachplanMediaService extends IService<TeachplanMedia> {

}
