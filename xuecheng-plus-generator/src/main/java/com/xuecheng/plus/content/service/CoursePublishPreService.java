package com.xuecheng.plus.content.service;

import com.xuecheng.plus.content.model.po.CoursePublishPre;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程发布 服务类
 * </p>
 *
 * @author itcast
 * @since 2023-03-19
 */
public interface CoursePublishPreService extends IService<CoursePublishPre> {

}
