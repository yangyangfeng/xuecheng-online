package com.xuecheng.plus.content.service;

import com.xuecheng.plus.content.model.po.CourseMarket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程营销信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2023-03-19
 */
public interface CourseMarketService extends IService<CourseMarket> {

}
