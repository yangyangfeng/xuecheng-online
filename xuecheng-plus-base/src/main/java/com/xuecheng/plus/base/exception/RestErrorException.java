package com.xuecheng.plus.base.exception;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.base.exception
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-26  08:08
 * @Description: 错误信息封装类
 * @Version: 1.0
 **/
public class RestErrorException extends RuntimeException{
    /**
     * 异常信息
     */
    private String errMessage;


    public RestErrorException(String errMessage) {
        this.errMessage = errMessage;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }
}
