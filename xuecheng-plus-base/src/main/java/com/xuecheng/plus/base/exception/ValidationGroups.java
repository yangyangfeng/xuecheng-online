package com.xuecheng.plus.base.exception;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.base.exception
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-26  10:22
 * @Description: TODO 用于分组校验，定义一些常用组
 * @Version: 1.0
 **/
public class ValidationGroups {
    public interface Inster{};
    public interface Update{};
    public interface Delete{};
}
