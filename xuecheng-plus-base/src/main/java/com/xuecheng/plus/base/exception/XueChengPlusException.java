package com.xuecheng.plus.base.exception;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.plus.base.exception
 * @Author: 枫逸舒
 * @CreateTime: 2023-03-26  09:26
 * @Description: TODO 项目自定义异常类
 * @Version: 1.0
 **/
public class XueChengPlusException extends RuntimeException{

    private String errMessage;

    public XueChengPlusException(){
        super();
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public XueChengPlusException(String errMessage) {
        super(errMessage);
        this.errMessage = errMessage;
    }

    public static void cast(String message){
        throw new XueChengPlusException(message);
    }
    public static void cast(CommonError error){
        throw new XueChengPlusException(error.getErrMessage());
    }

}
