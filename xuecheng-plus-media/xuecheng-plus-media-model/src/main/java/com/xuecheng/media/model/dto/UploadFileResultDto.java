package com.xuecheng.media.model.dto;

import com.xuecheng.media.model.po.MediaFiles;
import lombok.Data;
import lombok.ToString;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.media.model.dto
 * @Author: 枫逸舒
 * @CreateTime: 2023-04-01  12:13
 * @Description: TODO 上传文件返回结果
 * @Version: 1.0
 **/
@Data
@ToString
public class UploadFileResultDto extends MediaFiles {
}
