package com.xuecheng.media.model.dto;

import lombok.Data;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.media.model.dto
 * @Author: 枫逸舒
 * @CreateTime: 2023-04-01  15:54
 * @Description: TODO 上传普通文件请求参数
 * @Version: 1.0
 **/
@Data
public class UploadFileParamsDto {
    /**
     * 文件名称
     */
    private String filename;


    /**
     * 文件类型（文档，音频，视频）
     */
    private String fileType;
    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 标签
     */
    private String tags;
    /**
     * 上传人
     */
    private String username;
    /**
     * 备注
     */
    private String remark;
}
