package com.xuecheng.media.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: xuecheng-online
 * @BelongsPackage: com.xuecheng.media.config
 * @Author: 枫逸舒
 * @CreateTime: 2023-04-01  12:00
 * @Description: TODO  Minio配置类
 * @Version: 1.0
 **/
@Configuration
public class MinioConfig {

    // @Value 读取配置文件的注解

    @Value("${minio.endpoint}")
    private String endpoint;

    @Value("${minio.accessKey}")
    private String accessKey;

    @Value("${minio.secretKey}")
    private String secretKey;

    /**
     * 定义一个bean
     *
     * @return
     */
    @Bean
    public MinioClient minioClient() {
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint(endpoint)
                        .credentials(accessKey, secretKey)
                        .build();
        return minioClient;
    }
}
